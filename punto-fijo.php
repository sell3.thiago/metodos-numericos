<!DOCTYPE html>
<html>

<?php include 'include/head.php'; ?>

<body class="theme-teal">

    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Cargando</p>
        </div>
    </div>


    <div class="overlay"></div>
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">                
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index">METODOS NUMERICOS</a>
            </div>
        </div>
    </nav>


    <?php include 'include/menu.php'; ?>

    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="card">
                        <div class="header" align="center">
                            <strong>METODO DE PUNTO FIJO</strong>
                        </div>
                        <div class="body">
                            <form method="post">
                                <div class="form-line">
                                    <input type="text" name="__FUNCION" placeholder="Digite función despejada" required class="form-control">
                                </div>
                                <br>
                                <div class="form-line">
                                    <input type="text" name="__A" placeholder="Digite valor de X0" required class="form-control">
                                </div>
                                <br>
                                <div class="form-line">
                                    <input type="text" name="iteraciones" placeholder="Digite numero de iteraciones" required class="form-control">
                                </div>

                                <br>
                                <div class="form-line">
                                    <button class="btn bg-teal waves waves-effect" name="submit">Calcular</button>
                                </div>
                            </form>

                            <br><br>

                            <div class="table-responsive">
                                <?php
                                    if (isset($_POST["submit"])) {
                                        $x0 = $_POST["__A"];
                                        $it = $_POST["iteraciones"];

                                          function fnEval($x, $funcion)
                                    {
                                        $resultado=0;
                                        $funcion= str_replace("x","(".$x.")" , $funcion);
                                        eval("\$resultado=".$funcion.";");
                                        return $resultado;
                                    }
                                       

                                    ?>
                                        <table border="1" class="table table-bordered table-hover table-responsive">
                                            <thead>
                                                <th>I</th>
                                                <th>X0</th>
                                                <th>X1</th>
                                                <th>Error</th>
                                            </thead>
                                            <tr>
                                        <?php

                                        $err="";
                                       

                                        for ($n=0; $n < $it; $n++) {
                                         
                                            $x=$x0;
                                            $x1=fnEval($x,$_POST["__FUNCION"]);

                                            echo "<td>".$n."</td><td>".$x0."</td><td>".$x1."</td><td>".$err."</tr></tr>";
                                            
                                            $err=abs($x0-$x1);
                                            $x0=$x1;                                        
                                        }
                                        echo "</table>";

                                    }

                                    ?>
                            </div>  
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </section>


    <?php include 'include/script.php'; ?>
</body>

</html>
