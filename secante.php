<!DOCTYPE html>
<html>

<?php include 'include/head.php'; ?>

<body class="theme-teal">

    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Cargando</p>
        </div>
    </div>


    <div class="overlay"></div>
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">                
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index">METODOS NUMERICOS</a>
            </div>
        </div>
    </nav>


    <?php include 'include/menu.php'; ?>

    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="card">
                        <div class="header" align="center">
                            <strong>METODO DE LA SECANTE</strong>
                        </div>
                        <div class="body">
                            <?php error_reporting (0);?>
                            <form method="post">
                                <div class="form-line">
                                    <input type="text" name="__FUNCION" placeholder="Digite función (Ejemplo: X^2 + 2)" required class="form-control">
                                </div>
                                <br>
                                <div class="form-line">
                                    <input type="text" name="__A" placeholder="Digite valor de X0" required class="form-control">
                                </div>
                                <br>
                                <div class="form-line">
                                    <input type="text" name="__B" placeholder="Digite valor de  X1" required class="form-control">
                                </div>
                                <br>
                                <div class="form-line">
                                    <input type="text" name="iteraciones" placeholder="Digite iteraciones" required class="form-control">
                                </div>
                                <br>
                                <div class="form-line">
                                    <button class="btn bg-teal waves waves-effect" name="calcular">Calcular</button>
                                </div>
                            </form>
                            <br><br>
                            <div class="table-responsive">
                            <?php
                                if (isset($_POST["calcular"])) {
                                    $x1 = $_POST["__B"];
                                    $x0 = $_POST["__A"];
                                    $it = $_POST["iteraciones"];

                                      function fnEval($x, $funcion)
                                {
                                    $resultado=0;
                                    $funcion= str_replace("x","(".$x.")" , $funcion);
                                    eval("\$resultado=".$funcion.";");
                                    return $resultado;
                                }
                                   

                                ?>
                                    <table class="table table-responsive table-hover">
                                        <thead>
                                            <th>I</th>
                                            <th>X0</th>
                                            <th>X1</th>
                                            <th>Xi</th>
                                            <th>F(X1)</th>
                                            <th>F(X2)</th>
                                            <th>F(Xi)</th>
                                            <th>Error</th>
                                        </thead>
                                        <tr>
                                    <?php
                                    $err="";
                                    $pn = array();
                                  

                                    for ($n=1; $n <= $it; $n++) {
                                     
                                     $fx1=fnEval($x1,$_POST["__FUNCION"]);
                                      $fx0=fnEval($x0,$_POST["__FUNCION"]);
                                   
                                     $x2= $x1-((($x1-$x0) * $fx1) / ( $fx1 - $fx0 ));
                                     $fxi=fnEval($x2,$_POST["__FUNCION"]);

                                echo "<td>".$n."</td><td>".$x0."</td><td>".$x1."</td><td>".$x2."</td><td>".$fx0."</td><td>".$fx1."</td><td>".$fxi."</td><td>".$err."</tr></tr>";
                                    
                                     $pn[$n]=$x2;
                                     $x0=$x1;
                                     $x1=$x2;
                                     $err=($pn[$n]-$pn[$n-1]);


                                      
                                        
                                    
                                    }
                                    echo "</table>";

                                }

                                ?>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </section>


    <?php include 'include/script.php'; ?>
</body>

</html>
