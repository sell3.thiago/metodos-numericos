<section>
    <aside id="leftsidebar" class="sidebar">
        <div class="user-info">
            <div class="image">
                <img src="images/user.png" width="48" height="48" alt="User" />
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <strong>Universidad de Córdoba</strong>
                </div>
                <div class="email">Metodos númericos</div>
            </div>
        </div>


        <div class="menu">
            <ul class="list">
                <li class="header">METODOS</li>
                <li class="active">
                    <a href="index">
                        <i class="material-icons">home</i>
                        <span>Inicio</span>
                    </a>
                </li>
                <li>
                    <a href="biseccion">
                        <i class="material-icons">check</i>
                        <span>Bisección</span>
                    </a>
                </li>
                <li>
                    <a href="regla-falsa">
                        <i class="material-icons">check</i>
                        <span>Regla falsa</span>
                    </a>
                </li>

                <li>
                    <a href="newton">
                        <i class="material-icons">check</i>
                        <span>Newton Rapshon</span>
                    </a>
                </li>

                <li>
                    <a href="secante">
                        <i class="material-icons">check</i>
                        <span>Secante</span>
                    </a>
                </li>

                <li>
                    <a href="punto-fijo">
                        <i class="material-icons">check</i>
                        <span>Punto Fijo</span>
                    </a>
                </li>

                <li>
                    <a href="jacobi">
                        <i class="material-icons">check</i>
                        <span>Jacobi</span>
                    </a>
                </li>

                <li>
                    <a href="gauus-seidel">
                        <i class="material-icons">check</i>
                        <span>Gauus Seidel</span>
                    </a>
                </li>
            </ul>
        </div>
    </aside>
</section>