<!DOCTYPE html>
<html>

<?php include 'include/head.php'; ?>

<body class="theme-teal">

    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Cargando</p>
        </div>
    </div>


    <div class="overlay"></div>
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">                
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index">METODOS NUMERICOS</a>
            </div>
        </div>
    </nav>


    <?php include 'include/menu.php'; ?>

    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="card">
                        <div class="header" align="center">
                            <strong>METODO DE LA REGLA FALSA</strong>
                        </div>
                        <div class="body">
                            <form method="post">
                                <div class="form-line">
                                    <input type="text" name="__FUNCION" placeholder="Digite función (Ejemplo: X^2 + 2)" required class="form-control">
                                </div>
                                <br>
                                <div class="form-line">
                                    <input type="text" name="__A" placeholder="Digite valor de A" required class="form-control">
                                </div>
                                <br>
                                <div class="form-line">
                                    <input type="text" name="__B" placeholder="Digite función" required class="form-control">
                                </div>

                                <br>
                                <div class="form-line">
                                    <input type="text" name="iter" placeholder="Digite Error" required class="form-control">
                                </div>
                                <br>
                                <div class="form-line">
                                    <button class="btn bg-teal waves waves-effect" type="submit" name="submit">Calcular</button>
                                </div>
                            </form>
                            <br><br>

                            <div class="table-responsive">
                                <?php
                                    if (isset($_POST["submit"])) {
                                        $an = $_POST["__A"];
                                        $bn = $_POST["__B"];
                                        $e = $_POST["iter"];
                                        ?>
                                        <table class="table table-hover table-resonsive">
                                            <tr>
                                                <th>I</th>
                                                <th>A</th>
                                                <th>B</th>
                                                <th>Xi</th>
                                                <th>F(Xi)</th>
                                                <th>Error</th>
                                            </tr>
                                            <tr>
                                        <?php
                                      function fnEval($x, $funcion)
                                    {
                                        $resultado=0;
                                        $funcion= str_replace("x","(".$x.")" , $funcion);
                                        eval("\$resultado=".$funcion.";");
                                        return $resultado;
                                    }
                                    $funa = fnEval($an,$_POST['__FUNCION']);
                                    $funb = fnEval($bn,$_POST['__FUNCION']);

                                        for ($i=0; $i<$e ; $i++) { 
                                             
                                         $c = ($bn*$funa-$an*$funb)/($funa-$funb);  
                                           $func= fnEval($c,$_POST['__FUNCION']);

                                            echo "<td>".$i."</td><td>".$an."</td><td>".$bn."</td><td>".$c."</td><td>".$func."</td><td>".abs($bn-$an)."</tr></tr>";
                                             
                                           if (($funa*$func) <=0 )  {
                                               $bn = $c;
                                               $funb = $func;
                                            } else {
                                                $an = $c;
                                                $funa = $func;
                                            }
                                        }
                                     echo "</table>";

                                    }
                                ?>

                            </div>
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </section>


    <?php include 'include/script.php'; ?>
</body>

</html>
