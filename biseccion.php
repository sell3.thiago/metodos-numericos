<!DOCTYPE html>
<html>

<?php include 'include/head.php'; ?>

<body class="theme-teal">

    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Cargando</p>
        </div>
    </div>


    <div class="overlay"></div>
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">                
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index">METODOS NUMERICOS</a>
            </div>
        </div>
    </nav>


    <?php include 'include/menu.php'; ?>

    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="card">
                        <div class="header" align="center">
                            <strong>METODO DE BISECCIÓN</strong>
                        </div>
                        <div class="body">
                            <form method="post">
                                <div class="form-line">
                                    <input type="text" name="__FUNCION" placeholder="Digite función (Ejemplo: X^2 + 2)" required class="form-control">
                                </div>
                                <br>
                                <div class="form-line">
                                    <input type="text" name="__A" placeholder="Digite valor de A" required class="form-control">
                                </div>
                                <br>
                                <div class="form-line">
                                    <input type="text" name="__B" placeholder="Digite función" required class="form-control">
                                </div>

                                <br>
                                <div class="form-line">
                                    <input type="text" name="iter" placeholder="Digite numero interaciones" required class="form-control">
                                </div>
                                <br>
                                <div class="form-line">
                                    <button class="btn bg-teal waves waves-effect" type="submit" name="submit">Calcular</button>
                                </div>
                            </form>
                            <br><br>

                            <div class="table-responsive">
                                <?php
                                    if (isset($_POST["submit"])) {
                                        $an = $_POST["__A"];
                                        $bn = $_POST["__B"];
                                        $it = $_POST["iter"];
                                        ?>
                                        <table border="1" class="table table-bordered table-hover table-responsive">
                                            <thead>
                                                <th>I</th>
                                                <th>A</th>
                                                <th>B</th>
                                                <th>Xi</th>
                                                <th>F(Xi)</th>
                                                <th>Error</th>
                                            </thead>
                                            <tr>
                                        <?php
                                        $err="";
                                        $pn = array();
                                        for ($n=1; $n<=$it; $n++) {
                                            $eva = ($an + $bn)/2;
                                            $fun = str_replace("x",$eva,$_POST["__FUNCION"]);
                                            eval("\$foo = $fun;");
                                            $pn[]=$eva;
                                            if ($n>1) {
                                                $err = abs(($eva - $pn[$n-2])/$eva);
                                            }
                                            echo "<td>".$n."</td><td>".$an."</td><td>".$bn."</td><td>".$eva."</td><td>".$foo."</td><td>".$err."</tr></tr>";
                                            if ($foo<0 AND $err<0 )  {
                                               $bn = $eva;
                                            } elseif ($foo>0 AND $err>0 )  {
                                               $bn = $eva;
                                            } else {
                                                $an = $eva;
                                            }

                                        }
                                        echo "</table>";

                                    }
                                    ?>
                            </div>

                        </div>
                    </div>    
                </div>
                
            </div>
        </div>
    </section>



    <?php include 'include/script.php'; ?>
</body>

</html>
