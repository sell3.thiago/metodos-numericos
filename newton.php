<!DOCTYPE html>
<html>

<?php include 'include/head.php'; ?>

<body class="theme-teal">

    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Cargando</p>
        </div>
    </div>


    <div class="overlay"></div>
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">                
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index">METODOS NUMERICOS</a>
            </div>
        </div>
    </nav>


    <?php include 'include/menu.php'; ?>

    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix"><div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="card">
                        <div class="header" align="center">
                            <strong>METODO DE NEWTON RAPSHON</strong>
                        </div>
                        <div class="body">
                            <form method="post">
                                <div class="form-line">
                                    <input type="text" name="__FUNCION" placeholder="Digite función (Ejemplo: X^2 + 2)" required class="form-control">
                                </div>
                                <br>
                                <div class="form-line">
                                    <input type="text" name="_-DERIVADA" placeholder="Digite funcion derivada" required class="form-control">
                                </div>
                                <br>
                                <div class="form-line">
                                    <input type="text" name="x0" placeholder="Digite X0" required class="form-control">
                                </div>
                                <br>
                                <div class="form-line">
                                    <input type="text" name="tolerancia" placeholder="Digite tolerancia" required class="form-control">
                                </div>
                                <br>
                                <div class="form-line">
                                    <button class="btn bg-teal waves waves-effect" name="submit">Calcular</button>
                                </div>
                            </form>
                            <br><br>
                            <div class="table-responsive">
                                <?php
                                    function fnEval($x, $funcion)
                                    {
                                        $resultado=0;
                                       $funcio = str_replace ( "x" , $x , $funcion );
                                      eval('$resultado = '.$funcio.';');
                                        return $resultado;

                                    }

                                    if (isset($_POST["submit"])) {
                                        $a = $_POST["x0"];
                                        $it = $_POST["tolerancia"];
                                        $fn = $_POST["__FUNCION"];
                                        $fnd = $_POST["_-DERIVADA"];
                                        ?>
                                        <table border="1" class="table table-bordered">
                                            <tr>
                                                <td>I</td>
                                                <td>X0</td>
                                                <td>F(Xi)</td>
                                                <td>F`(Xi)</td>
                                                <td>Error</td>
                                            </tr>
                                            <tr>
                                        <?php
                                        $err=0;
                                        $pn = array();
                                      

                                        for ($n=0; $n<$it; $n++) {
                                          if ($n == 0) {
                                            $aa = $a;
                                            $fun1= fnEval($a,$fn);
                                            $fund1= fnEval($a,$fnd);
                                            $err = 0;

                                            echo "<td>".$n."</td><td>".$a."</td><td>".$fun1."</td><td>".$fund1."</td><td>".$err."</tr></tr>";
                                            
                                          }else{
                                             $a = $a-($fun1/$fund1);
                                             $fun1= fnEval($a,$fn);
                                            $fund1= fnEval($a,$fnd);
                                            $err = ($a - $aa) / $a;

                                            echo "<td>".$n."</td><td>".$a."</td><td>".$fun1."</td><td>".$fund1."</td><td>".$err."</tr></tr>";
                                            
                                                   
                                          }
                                        }
                                        echo "</table>";

                                    }

                                    ?>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </section>


    <?php include 'include/script.php'; ?>
</body>

</html>
