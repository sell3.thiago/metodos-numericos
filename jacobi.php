<!DOCTYPE html>
<html>

<?php include 'include/head.php'; ?>

<body class="theme-teal">

    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Cargando</p>
        </div>
    </div>


    <div class="overlay"></div>
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">                
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index">METODOS NUMERICOS</a>
            </div>
        </div>
    </nav>


    <?php include 'include/menu.php'; ?>

    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix"><div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="card">
                        <div class="header" align="center">
                            <strong>METODO DE JACOBI</strong>
                        </div>
                        <div class="body">
                            <form method="post">
                                <div class="form-line">
                                    <input type="text" name="__FUNCIONX" placeholder="Digite función X" required class="form-control">
                                </div>
                                <br>
                                <div class="form-line">
                                    <input type="text" name="__FUNCIONY" placeholder="Digite función Y" required class="form-control">
                                </div>
                                <br>
                                <div class="form-line">
                                    <input type="text" name="__FUNCIONZ" placeholder="Digite función Z" required class="form-control">
                                </div>
                                <br>
                                <div class="form-line">
                                    <input type="text" name="x0" placeholder="Digite valor inicial X0" required class="form-control">
                                </div>
                                <br>
                                <div class="form-line">
                                    <button class="btn bg-teal waves waves-effect" name="calcular">Calcular</button>
                                </div>
                            </form>
                            <br><br>
                            <div class="table-responsive">
                                <?php
                                    if (isset($_POST["calcular"])) {
                                        
                                        $x=$_POST["x0"];
                                        $z = $_POST["x0"];
                                        $y=$_POST["x0"];

                                        function fnEvalx($y,$z, $funcion)
                                        {
                                            $resultado=0;
                                           $funcio = str_replace ( "y" ,$y,$funcion );
                                           $funcio1 = str_replace ( "z" ,$z,$funcio );
                                          eval('$resultado = '.$funcio1.';');
                                            return $resultado;

                                        }
                                        
                                        function fnEvaly($x,$z, $funcion)
                                        {
                                            $resultado=0;
                                           $funcio = str_replace ( "x" ,$x,$funcion );
                                           $funcio1 = str_replace ( "z" ,$z,$funcio );
                                          eval('$resultado = '.$funcio1.';');
                                            return $resultado;


                                        }  
                                        
                                        function fnEvalz($x,$y, $funcion)
                                        {
                                            $resultado=0;
                                           $funcio = str_replace ( "x" ,$x,$funcion );
                                           $funcio1 = str_replace ( "y" ,$y,$funcio );
                                          eval('$resultado = '.$funcio1.';');
                                            return $resultado;


                                        }

                                    ?>
                                        <table  class="table table-bordered">
                                            <tr><td>I</td>
                                                <td>X</td>
                                                <td>Y</td>
                                                 <td>Z</td>
                                            
                                            </tr>
                                            <tr>
                                        <?php

                                        $err="";
                                        $tx=$x; 
                                        $ty=$y;
                                        $tz=$z;
                                        $err = 0.000001;
                                         $n=0;
                                        do {
                                         
                                      $x=$tx;
                                      $y=$ty;
                                      $z=$tz;

                                     $tx = fnEvalx($y,$z,$_POST["__FUNCIONX"]);
                                     $ty = fnEvaly($x, $z,$_POST["__FUNCIONY"]);
                                     $tz = fnEvalz($x, $y,$_POST["__FUNCIONZ"]);
                                     
                                       
                                      $n++;

                                      echo "<td>".$n."</td><td>".$x."</td><td>".$y."</td><td>".$z."</tr></tr>";
                                        
                                       
                                          } while ((($tx-$x)/$tx)>$err || (($ty-$y)/$ty)>$err || (($tz-$z)/$tz)>$err); {
                                         


                                          
                                            
                                        echo "</table>";
                                        }
                                        

                                    }

                                    ?>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </section>


    <?php include 'include/script.php'; ?>
</body>

</html>
jacon